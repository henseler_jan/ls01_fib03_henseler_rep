﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {  while (true)
    	{
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	
    		double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    		fahrkartenAusgeben();
       		rueckgeldAusgeben(rueckgabebetrag);
    	}
    }
    
    public static double fahrkartenbestellungErfassen()
    {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	int anzahlTickets;
    	
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus.\n");
    	System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\nTageskarte Regeltarif AB [8,60 EUR] (2)\nKleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
    	System.out.println("Ihre Wahl:");
    	int wahl = tastatur.nextInt();
    	
    	switch(wahl) 
    	{
    	case 1:	
    		zuZahlenderBetrag = 2.90; 
    		break;
    	case 2: 
    		zuZahlenderBetrag = 8.60;
    		break;
    	case 3:	
    		zuZahlenderBetrag = 23.50;
    		break;
    	default: 
    		zuZahlenderBetrag = 2.90;
    	
    	}
        
    	System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();
        zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
        return zuZahlenderBetrag;
        
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag)
    {
    	// Geldeinwurf
        // -----------
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	double rueckgabebetrag = 0.0;
    	eingezahlterGesamtbetrag = 0.0;
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f€\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 0,05€, höchstens 2,00€): ");
     	
     	   eingeworfeneMünze = tastatur.nextDouble();
     	   eingezahlterGesamtbetrag += eingeworfeneMünze;
     	   rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
     	  
        }
    	return rueckgabebetrag;
    }
    
    public static void fahrkartenAusgeben()
    {
    	// Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgabebetrag)
    {
    	// Rückgeldberechnung und -Ausgabe
        // -------------------------------
        if(rueckgabebetrag > 0.0)
        {
     	   rueckgabebetrag = rueckgabebetrag * 100;
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f€\n", rueckgabebetrag/100);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 200) // 2 EURO-Münzen
            {
         	  System.out.println("2,00€");
 	          rueckgabebetrag -= 200;
            }
            while(rueckgabebetrag >= 100) // 1 EURO-Münzen
            {
         	  System.out.println("1,00€");
 	          rueckgabebetrag -= 100;
            }
            while(rueckgabebetrag >= 50) // 50 CENT-Münzen
            {
         	  System.out.println("0,50€");
 	          rueckgabebetrag -= 50;
            }
            while(rueckgabebetrag >= 20) // 20 CENT-Münzen
            {
         	  System.out.println("0,20€");
         	 rueckgabebetrag -= 20;
            }
            while(rueckgabebetrag >= 10) // 10 CENT-Münzen
            {
         	  System.out.println("0,10€");
         	 rueckgabebetrag -= 10;
            }
            while(rueckgabebetrag >= 5)// 5 CENT-Münzen
            {
         	  System.out.println("0,05€");
         	 rueckgabebetrag -= 5;
            }
            rueckgabebetrag = rueckgabebetrag / 100;
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n");
    }
}